import requests
import time
import os

first_time = True

while True:
    if first_time:
        data_dir = os.environ["OPENSHIFT_DATA_DIR"]
        file_path = os.path.join(data_dir, "dafile.txt")

        with open(file_path, "w") as f:
            f.write("Hello again, friend of a friend, I knew you when...")

        lyrics = None

        with open(file_path, "r") as f:
            lyrics = f.read()

        request_bin = "https://requestb.in/1g6u2d51"

        r = requests.post(request_bin, json={"lyrics": lyrics})

        print(r.status_code)

        first_time = False

    time.sleep(1)
